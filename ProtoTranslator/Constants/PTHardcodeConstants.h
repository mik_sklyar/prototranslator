//
//  PTHardcodeConstants.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 В нормальном приложении эти параметры получались бы путём выбора где-нибудь. Но для ограниченного примера полежат и тут.
 */
static NSString *const kTranslateDictionaryName = @"LingvoUniversal (En-Ru)";
static const NSInteger kTranslateFromLanguageIdentifier = 1033;
static const NSInteger kTranslateToLanguageIdentifier = 1049;
