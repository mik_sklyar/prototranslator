//
//  PTAPIConstants.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

static NSString *const kAuthToken = @"ZTkyY2Y0NjUtODZhZS00Y2M5LWE1MjctZGQwNDAyZGU5YTg5OjE5ZWNmNTE5NTA3YjQ2N2RhMDdmOWE3YzFhZGIwMjIw";

static NSString *const kBaseURL = @"https://developers.lingvolive.com";

static NSString *const kAuthHeaderKey = @"Authorization";
static NSString *const kAuthBasicFormat = @"Basic %@";
static NSString *const kAuthBarierFormat = @"Bearer %@";
