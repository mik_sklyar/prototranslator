//
//  PTL10nStrings.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 Здесь строки вынесены тоже для порядка. В нормальном приложении с локализацией явно будет другая структура ресурсов.
 */

static NSString *const kL10nOK = @"Лады";

static NSString *const kL10nErrorTitle = @"Ошибка";
static NSString *const kL10nErrorMessageCantTranslate = @"Не удалось перевести, попробуйте изменить слово.";

static NSString *const lL10nMainScreenTitle = @"Перевести";
static NSString *const lL10nTranslatePlaceholder = @"Введите что-нибудь по английски";
