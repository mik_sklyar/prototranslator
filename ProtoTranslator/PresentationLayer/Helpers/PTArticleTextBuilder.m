//
//  PTArticleTextBuilder.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 22/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PTArticleTextBuilder.h"
#import "PTArticleModel.h"

#import "PTArticleTextNode.h"
#import "PTArticleMarkupNode.h"
#import "PTArticleListNode.h"
#import "PTArticleListItemNode.h"
#import "PTArticleAbbrevNode.h"
#import "PTArticleSoundNode.h"
#import "PTArticleCardRefNode.h"

static const float kTitleFontSize = 20.f;
static const float kTextFontSize = 14.f;
static const float kTabIndent = 8.f;
static const float kParagraphSpacing = 6.f;

static NSString *const kParagraphSymbol = @"\n";
static NSString *const kSpeakerSymbol = @"🔊";
static NSString *const kTranscriptionFormat = @"[%@]";

@implementation PTArticleTextBuilder

- (NSAttributedString *)attributedTextWithModel:(PTArticleModel *)articleModel {
    NSMutableAttributedString *articleText = [[NSMutableAttributedString alloc] init];

    NSString *title = articleModel.title; //stringByAppendingString:kParagraphSymbol];
    NSMutableAttributedString *attrTitle =
    [[NSMutableAttributedString alloc] initWithString:title
                                           attributes:[self titleAttributes]];
    [attrTitle.mutableString appendString:kParagraphSymbol];

    [articleText appendAttributedString:attrTitle];

    NSAttributedString *text = [self attributedTextWithNodes:articleModel.bodyNodes
                                              baseAttributes:[self baseTextAttributes]];

    [articleText appendAttributedString:text];

    return [articleText copy];
}

#pragma mark - Private text building

- (NSAttributedString *)attributedTextWithNodes:(NSArray<PTArticleNode *> *)nodes
                                 baseAttributes:(NSDictionary *)attributes {
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] init];

    for (PTArticleNode *node in nodes) {
        NSAttributedString *nodeAttrString = nil;
        NSString *nodeString = nil;
        NSDictionary *nodeAttributes = attributes;

        if ([node isKindOfClass:[PTArticleTextNode class]]) {
            //Обычный текст

            PTArticleTextNode *textNode = (PTArticleTextNode *)node;
            nodeString = node.text;
            nodeAttributes = [self textAttributesWithBase:attributes
                                                   italic:textNode.isItalic
                                                   accent:textNode.isAccent];
        } else if ([node isKindOfClass:[PTArticleAbbrevNode class]]) {
            //Аббревиатура

            nodeString = node.text;
            nodeAttributes = [self abbrevAttributesWithBase:attributes];
        }  else if ([node isKindOfClass:[PTArticleSoundNode class]]) {
            //Значок звукового файла (пока просто значок, без кнопки/ссылки)

            nodeString = kSpeakerSymbol;
        }  else if ([node isKindOfClass:[PTArticleListNode class]]) {
            //Список

            nodeAttrString = [self attributedTextWithList:(PTArticleListNode *)node
                                           baseAttributes:attributes];
        }  else if ([node isKindOfClass:[PTArticleCardRefNode class]]) {
            //Ссылка на статью

            nodeString = node.text;
            nodeAttributes = [self referenceAttributesWithBase:attributes];
        } else if ([node isKindOfClass:[PTArticleMarkupNode class]]) {
            //Составные ноды

            PTArticleMarkupNode *markupNode = (PTArticleMarkupNode *)node;

            if (node.nodeType == PTArticleNodeTypeParagraph) {
                //Параграф с переводом строки в конце

                NSMutableAttributedString *parNodeText = [[self attributedTextWithNodes:markupNode.markup
                                                                         baseAttributes:attributes] mutableCopy];
                [parNodeText.mutableString appendString:kParagraphSymbol];
                nodeAttrString = [parNodeText copy];
            } else if (node.nodeType == PTArticleNodeTypeComment) {
                //Комментарий

                nodeAttrString = [self attributedTextWithNodes:markupNode.markup
                                                baseAttributes:[self commentAttributesWithBase:attributes]];
            } else if (node.nodeType == PTArticleNodeTypeExample) {
                //Пример

                nodeAttrString = [self attributedTextWithNodes:markupNode.markup
                                                baseAttributes:[self exampleAttributesWithBase:attributes]];
            }
        } else if ([node isKindOfClass:[PTArticleNode class]]) {
            //Простые ноды

            if (node.nodeType == PTArticleNodeTypeTranscription) {
                //Транскрипция

                nodeString = [NSString stringWithFormat:kTranscriptionFormat, node.text];
                nodeAttributes = [self transcriptionAttributesWithBase:attributes];
            } else if (node.nodeType == PTArticleNodeTypeCaption) {
                //Заголовок
                nodeString = [node.text stringByAppendingString:@" "];
                nodeAttributes = [self captionAttributesWithBase:attributes];
            } else {
                NSLog(@"WARNING!!! Unhandeled node type!");
            }
        }

        if (nodeString.length > 0) {
            nodeAttrString = [[NSAttributedString alloc] initWithString:nodeString attributes:nodeAttributes];
        }
        if (nodeAttrString.length > 0) {
            [text appendAttributedString:nodeAttrString];
        }
    }

    return [text copy];
}

- (NSAttributedString *)attributedTextWithList:(PTArticleListNode *)listNode
                                 baseAttributes:(NSDictionary *)attributes {
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] init];

    NSInteger nodeIndex = 1;
    for (PTArticleListItemNode *node in listNode.listItems) {
        NSAssert([node isKindOfClass:[PTArticleListItemNode class]], @"Incorrect node type!!!");

        NSDictionary *nodeTextAttributes = [self listItemAttributesWithBase:attributes listType:listNode.listType color:nil];
        NSAttributedString *nodeAttrString = [self attributedTextWithNodes:node.markup baseAttributes:nodeTextAttributes];

        if (nodeAttrString.length > 0) {
            if (listNode.nodeType == PTArticleNodeTypeList) {
                //Цифры ставим только обычным спискам
                NSAttributedString *listPoint = [self attributedPointWithListType:listNode.listType
                                                                        itemIndex:nodeIndex baseAttributes:attributes];
                [text appendAttributedString:listPoint];
            }
            [text appendAttributedString:nodeAttrString];

            if (listNode.nodeType == PTArticleNodeTypeExamples || listNode.nodeType == PTArticleNodeTypeCardRefs) {
                //В конце примеров и списка ссылок на статьи нужно перенести строку, а то не красиво
                [text.mutableString appendString:kParagraphSymbol];
            }
            nodeIndex++;
        }
    }

    return [text copy];
}

- (NSAttributedString *)attributedPointWithListType:(NSInteger)listType
                                          itemIndex:(NSInteger)itemIndex
                                     baseAttributes:(NSDictionary *)attributes {
    NSString *itemPointString = nil;

    //TODO: переделать всё нормально. NSNumberFormatter?
    if (listType == 1) {
        itemPointString = kParagraphSymbol;
    } else if (listType == 2) {
        itemPointString = [NSString stringWithFormat:@"%ld. ", (long)itemIndex];
    } else if (listType == 3) {
        itemPointString = [NSString stringWithFormat:@"%ld) ", (long)itemIndex];
    } else if (listType == 4) {
        //TODO: вынести и сделать как надо с ограничениями, локализацией и т.п.
        static NSString *abc = @"?абвгдеёжзиклмнопрстуфхцчшщъыьэюя";
        itemPointString = [NSString stringWithFormat:@"%@) ", [abc substringWithRange:(NSRange){itemIndex, 1}]];
    }
    if (itemPointString.length == 0) {
        itemPointString = @"";
    }

    NSDictionary *nodeAttributes = [self listItemAttributesWithBase:attributes
                                                           listType:listType
                                                              color:[UIColor lightGrayColor]];

    return [[NSAttributedString alloc] initWithString:itemPointString
                                           attributes:nodeAttributes];
}

#pragma mark - Attributes for different node types

- (NSDictionary *)titleAttributes {
    return @{
             NSFontAttributeName: [UIFont systemFontOfSize:kTitleFontSize],
             NSForegroundColorAttributeName: [UIColor colorWithRed:130.f/255.f green:177.f/255.f blue:255.f/255.f alpha:1.f],
             };
}

- (NSDictionary *)baseTextAttributes {
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.paragraphSpacing = kParagraphSpacing;

    return @{NSParagraphStyleAttributeName: [paragraphStyle copy],
             NSFontAttributeName: [UIFont systemFontOfSize:kTextFontSize],
             NSForegroundColorAttributeName: [UIColor blackColor]};
}

- (NSDictionary *)textAttributesWithBase:(NSDictionary *)attributes italic:(BOOL)italic accent:(BOOL)accent {
    if (!italic && !accent) {
        return attributes;
    }
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    UIFont *font = mAttributes[NSFontAttributeName];
    if (italic) {
        mAttributes[NSFontAttributeName] = [UIFont italicSystemFontOfSize:font.pointSize];
        mAttributes[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    }
    if (accent) {
        mAttributes[NSFontAttributeName] = [UIFont boldSystemFontOfSize:font.pointSize];
        mAttributes[NSForegroundColorAttributeName] = [UIColor lightGrayColor];
    }
    return [mAttributes copy];
}

- (NSDictionary *)abbrevAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSForegroundColorAttributeName] = [UIColor grayColor];
    return [mAttributes copy];
}

- (NSDictionary *)commentAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    return [mAttributes copy];
}

- (NSDictionary *)exampleAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSForegroundColorAttributeName] = [UIColor darkGrayColor];
    return [mAttributes copy];
}

- (NSDictionary *)transcriptionAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSForegroundColorAttributeName] = [UIColor blueColor];
    return [mAttributes copy];
}

- (NSDictionary *)captionAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSFontAttributeName] = [UIFont boldSystemFontOfSize:[mAttributes[NSFontAttributeName] pointSize]];
    return [mAttributes copy];
}

- (NSDictionary *)referenceAttributesWithBase:(NSDictionary *)attributes {
    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSForegroundColorAttributeName] = [UIColor blueColor];
    mAttributes[NSUnderlineStyleAttributeName] = @(NSUnderlineStyleSingle);
    return [mAttributes copy];
}

- (NSDictionary *)listItemAttributesWithBase:(NSDictionary *)attributes listType:(NSInteger)listType color:(UIColor *)color {
    NSMutableParagraphStyle *style = [(attributes[NSParagraphStyleAttributeName] ?:
                                      [NSParagraphStyle defaultParagraphStyle]) mutableCopy];
    if (listType > 1) {
        style.firstLineHeadIndent = kTabIndent * (listType - 1);
        style.headIndent = style.firstLineHeadIndent;
    }

    NSMutableDictionary *mAttributes = [attributes mutableCopy];
    mAttributes[NSParagraphStyleAttributeName] = [style copy];
    if (color) {
        mAttributes[NSForegroundColorAttributeName] = color;
    }
    return [mAttributes copy];
}

@end
