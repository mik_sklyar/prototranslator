//
//  PTArticleTextBuilder.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 22/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PTArticleModel;

/**
 Компоновщик текста. Подготавливает красивый текст по модельке статьи.
 */
@interface PTArticleTextBuilder : NSObject

- (NSAttributedString *)attributedTextWithModel:(PTArticleModel *)articleModel;

@end
