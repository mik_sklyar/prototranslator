//
//  PTHistoryListInteractor.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListInteractorInput.h"

@protocol PTHistoryListInteractorOutput;
@protocol PTCacheService;

@interface PTHistoryListInteractor : NSObject <PTHistoryListInteractorInput>

@property (nonatomic, weak) id<PTHistoryListInteractorOutput> output;

@property (nonatomic, strong) id<PTCacheService> cacheService;

@end
