//
//  PTHistoryListInteractor.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListInteractor.h"
#import "PTHistoryListInteractorOutput.h"

#import "PTCacheService.h"

@implementation PTHistoryListInteractor

#pragma mark - Методы PTHistoryListInteractorInput

- (void)obtainTranslatedWordsWithCompletion:(void (^)(NSArray<NSString *> *words))completion {
    [self.cacheService loadAllArticlesKeysWithCompletionBlock:^(NSArray<NSString *> *articlesKeys, NSError *error) {
        if (!completion) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(articlesKeys);
        });
    }];
}

@end
