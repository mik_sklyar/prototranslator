//
//  PTHistoryListInteractorInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTHistoryListInteractorInput <NSObject>

- (void)obtainTranslatedWordsWithCompletion:(void (^)(NSArray<NSString *> *words))completion;

@end
