//
//  PTHistoryListPresenter.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListPresenter.h"
#import "PTHistoryListViewInput.h"
#import "PTHistoryListInteractorInput.h"
#import "PTHistoryListRouterInput.h"
#import "PTHistoryListModuleOutput.h"

@implementation PTHistoryListPresenter

#pragma mark - Методы PTHistoryListModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы PTHistoryListViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)didTriggerViewWillAppear {
    [self.interactor obtainTranslatedWordsWithCompletion:^(NSArray<NSString *> *words) {
        [self.view configureWithWords:words];
    }];
}

- (void)didSelectWord:(NSString *)word {
    [self.moduleOutput historyListDidSelectWord:word];

    [self.router closeModuleAnimated:YES];
}

#pragma mark - Методы PTHistoryListInteractorOutput

@end
