//
//  PTHistoryListPresenter.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListViewOutput.h"
#import "PTHistoryListInteractorOutput.h"
#import "PTHistoryListModuleInput.h"

@protocol PTHistoryListViewInput;
@protocol PTHistoryListInteractorInput;
@protocol PTHistoryListRouterInput;
@protocol PTHistoryListModuleOutput;

@interface PTHistoryListPresenter : NSObject <PTHistoryListModuleInput, PTHistoryListViewOutput, PTHistoryListInteractorOutput>

@property (nonatomic, weak) id<PTHistoryListViewInput> view;
@property (nonatomic, strong) id<PTHistoryListInteractorInput> interactor;
@property (nonatomic, strong) id<PTHistoryListRouterInput> router;
@property (nonatomic, weak) id<PTHistoryListModuleOutput> moduleOutput;

@end
