//
//  PTHistoryListModuleOutput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol PTHistoryListModuleOutput <RamblerViperModuleOutput>

- (void)historyListDidSelectWord:(NSString *)word;

@end
