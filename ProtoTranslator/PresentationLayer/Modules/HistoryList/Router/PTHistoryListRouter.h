//
//  PTHistoryListRouter.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface PTHistoryListRouter : NSObject <PTHistoryListRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
