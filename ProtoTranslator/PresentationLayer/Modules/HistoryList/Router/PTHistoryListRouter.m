//
//  PTHistoryListRouter.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "PTHistoryListRouter.h"

@implementation PTHistoryListRouter

#pragma mark - Методы PTHistoryListRouterInput

- (void)closeModuleAnimated:(BOOL)animated {
    [self.transitionHandler closeCurrentModule:animated];
}

@end
