//
//  PTHistoryListViewController.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListViewController.h"
#import "PTHistoryListViewOutput.h"
#import "PTHistoryListTableManager.h"
@interface PTHistoryListViewController () <PTHistoryListTableManagerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation PTHistoryListViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.output didTriggerViewWillAppear];
}

#pragma mark - Методы PTHistoryListViewInput

- (void)setupInitialState {
    self.tableViewManager.delegate = self;
    self.tableView.delegate = [self.tableViewManager delegateForTableView:self.tableView];
    self.tableView.dataSource = [self.tableViewManager dataSourceForTableView:self.tableView];
}

- (void)configureWithWords:(NSArray<NSString *> *)words {
    [self.tableViewManager updateTableViewModelWithItems:words];
}

#pragma mark - DPDDSettingsTableViewManagerDelegate

- (void)didSelectWord:(NSString *)word {
    [self.output didSelectWord:word];
}

@end
