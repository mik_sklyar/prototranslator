//
//  PTHistoryListTableModel.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Очень упрощённая модель для данных таблицы. В общем случае, она обеспечивает полный набор модельных объектов для таблицы,
 включая ячейки, секции, хедеры, футеры и т.д.
 */
@interface PTHistoryListTableModel : NSObject <UITableViewDataSource>

- (void)updateWithArray:(NSArray *)array;

- (id)cellObjectAtIndexPath:(NSIndexPath *)indexPath;

- (void)registerCellClassesInTableView:(UITableView *)tableView;

@end
