//
//  PTHistoryListTableManager.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListTableManager.h"
#import "PTHistoryListTableModel.h"

@interface PTHistoryListTableManager () <UITableViewDelegate>

@property (nonatomic, strong) PTHistoryListTableModel *tableViewModel;
@property (nonatomic, weak) UITableView *tableView;

@end

@implementation PTHistoryListTableManager

- (PTHistoryListTableModel *)tableViewModel {
    if (!_tableViewModel) {
        _tableViewModel = [[PTHistoryListTableModel alloc] init];
    }
    return _tableViewModel;
}

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView {
    [self.tableViewModel registerCellClassesInTableView:tableView];
    return self.tableViewModel;
}

- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView {
    self.tableView = tableView;
    return self;
}


- (void)updateTableViewModelWithItems:(NSArray *)items {
    [self.tableViewModel updateWithArray:items];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    //Допущение. Так-то берём объект данных и из него получаем необходимую информацию.
    NSString *word = [self.tableViewModel cellObjectAtIndexPath:indexPath];

    [self.delegate didSelectWord:word];
}

@end
