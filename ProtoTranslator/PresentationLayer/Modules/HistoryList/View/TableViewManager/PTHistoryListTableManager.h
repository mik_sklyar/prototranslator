//
//  PTHistoryListTableManager.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PTHistoryListTableManagerDelegate

- (void)didSelectWord:(NSString *)word;

@end

@interface PTHistoryListTableManager : NSObject

@property (nonatomic, weak) id<PTHistoryListTableManagerDelegate> delegate;

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView;
- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView;

- (void)updateTableViewModelWithItems:(NSArray *)items;

@end
