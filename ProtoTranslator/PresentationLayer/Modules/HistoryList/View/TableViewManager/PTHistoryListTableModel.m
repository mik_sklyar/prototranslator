//
//  PTHistoryListTableModel.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTHistoryListTableModel.h"

static NSString *const kCommonReusableCellIdentifier = @"plain_cell";

@interface PTHistoryListTableModel ()

@property (nonatomic, strong) NSArray *cellObjects;

@end

@implementation PTHistoryListTableModel

- (void)updateWithArray:(NSArray *)array {
    self.cellObjects = array;
}

- (id)cellObjectAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellObjects[indexPath.row];
}

- (void)registerCellClassesInTableView:(UITableView *)tableView {
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCommonReusableCellIdentifier];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cellObject = [self cellObjectAtIndexPath:indexPath];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommonReusableCellIdentifier
                                                            forIndexPath:indexPath];

    //Допущение, т.к. мы никак не преобразуем данные, а храним просто строки.
    //А так то тут должен быть вызов метода, который полностью настраивает ячейку в соответствии с объектом данных.
    cell.textLabel.text = cellObject;

    return cell;
}

@end
