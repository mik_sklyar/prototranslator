//
//  PTHistoryListViewController.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PTHistoryListViewInput.h"

@protocol PTHistoryListViewOutput;
@class PTHistoryListTableManager;

@interface PTHistoryListViewController : UIViewController <PTHistoryListViewInput>

@property (nonatomic, strong) id<PTHistoryListViewOutput> output;

@property (nonatomic, strong) PTHistoryListTableManager *tableViewManager;

@end
