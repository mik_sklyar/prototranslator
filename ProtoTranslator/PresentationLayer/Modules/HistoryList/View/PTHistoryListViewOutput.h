//
//  PTHistoryListViewOutput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTHistoryListViewOutput <NSObject>

/**
 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

- (void)didTriggerViewWillAppear;

- (void)didSelectWord:(NSString *)word;

@end
