//
//  PTHistoryListViewInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTHistoryListViewInput <NSObject>

/**
 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

/**
 Настраивает вид экрана используя список слов

 @param words Ранее переведённые слова
 */
- (void)configureWithWords:(NSArray<NSString *> *)words;

@end
