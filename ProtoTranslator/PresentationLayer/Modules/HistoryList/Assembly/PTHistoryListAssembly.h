//
//  PTHistoryListAssembly.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol RamblerViperModuleFactoryProtocol;

@interface PTHistoryListAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<RamblerViperModuleFactoryProtocol>)factoryHistoryListModule;

@end
