//
//  PTHistoryListAssembly.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 25/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "PTHistoryListAssembly.h"
#import "PTHistoryListViewController.h"
#import "PTHistoryListInteractor.h"
#import "PTHistoryListPresenter.h"
#import "PTHistoryListRouter.h"

#import "PTServiceComponents.h"

#import "PTHistoryListTableManager.h"

@interface PTHistoryListAssembly ()

@property (nonatomic, strong, readonly) TyphoonAssembly<PTServiceComponents> *serviceComponents;

@end

@implementation PTHistoryListAssembly

- (id<RamblerViperModuleFactoryProtocol>)factoryHistoryListModule {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardHistoryListModule]];
                                                  [initializer injectParameterWith:@"PTHistoryListViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardHistoryListModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"HistoryList"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (PTHistoryListViewController *)viewHistoryList {
    return [TyphoonDefinition withClass:[PTHistoryListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterHistoryList]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterHistoryList]];
                              [definition injectProperty:@selector(tableViewManager)
                                                    with:[self tableManager]];
                          }];
}

- (PTHistoryListInteractor *)interactorHistoryList {
    return [TyphoonDefinition withClass:[PTHistoryListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterHistoryList]];
                              [definition injectProperty:@selector(cacheService)
                                                    with:[self.serviceComponents cacheService]];
                          }];
}

- (PTHistoryListPresenter *)presenterHistoryList{
    return [TyphoonDefinition withClass:[PTHistoryListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewHistoryList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorHistoryList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerHistoryList]];
                          }];
}

- (PTHistoryListRouter *)routerHistoryList{
    return [TyphoonDefinition withClass:[PTHistoryListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewHistoryList]];
                          }];
}

- (PTHistoryListTableManager *)tableManager {
    return [TyphoonDefinition withClass:[PTHistoryListTableManager class]];
}

@end
