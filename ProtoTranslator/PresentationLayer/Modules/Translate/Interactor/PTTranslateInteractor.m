//
//  PTTranslateInteractor.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateInteractor.h"
#import "PTTranslateInteractorOutput.h"

#import "PTTranslateService.h"
#import "PTCacheService.h"

#import "PTHardcodeConstants.h"

@implementation PTTranslateInteractor

#pragma mark - Методы PTTranslateInteractorInput

- (void)translateText:(NSString *)text completionBlock:(void (^)(PTArticleModel *article, NSError *error))completion {
    [self.translateService translateText:text
                              dictionary:kTranslateDictionaryName
                          fromLanguageID:@(kTranslateFromLanguageIdentifier)
                            toLanguageID:@(kTranslateToLanguageIdentifier)
                         completionBlock:^(PTArticleModel *article, NSError *error) {
                             if (completion) {
                                 completion(article, error);
                             }
                             if (error == nil) {
                                 [self.cacheService saveArticle:article];
                             }
                         }];
}

- (void)obtainCachedArticleForText:(NSString *)text withCompletionBlock:(void (^)(PTArticleModel *article, NSError *error))completion {
    [self.cacheService loadArticleByWord:text completionBlock:completion];
}

@end
