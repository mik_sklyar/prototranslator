//
//  PTTranslateInteractorInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PTArticleModel;

@protocol PTTranslateInteractorInput <NSObject>

- (void)translateText:(NSString *)text completionBlock:(void (^)(PTArticleModel *article, NSError *error))completion;

- (void)obtainCachedArticleForText:(NSString *)text withCompletionBlock:(void (^)(PTArticleModel *article, NSError *error))completion;

@end
