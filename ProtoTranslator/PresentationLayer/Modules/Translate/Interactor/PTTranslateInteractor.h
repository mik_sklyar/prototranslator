//
//  PTTranslateInteractor.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateInteractorInput.h"

@protocol PTTranslateInteractorOutput;
@protocol PTTranslateService;
@protocol PTCacheService;

@interface PTTranslateInteractor : NSObject <PTTranslateInteractorInput>

@property (nonatomic, weak) id<PTTranslateInteractorOutput> output;

@property (nonatomic, strong) id<PTTranslateService> translateService;
@property (nonatomic, strong) id<PTCacheService> cacheService;

@end
