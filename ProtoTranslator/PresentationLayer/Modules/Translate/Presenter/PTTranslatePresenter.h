//
//  PTTranslatePresenter.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateViewOutput.h"
#import "PTTranslateInteractorOutput.h"
#import "PTTranslateModuleInput.h"

@protocol PTTranslateViewInput;
@protocol PTTranslateInteractorInput;
@protocol PTTranslateRouterInput;

@class PTArticleTextBuilder;

@interface PTTranslatePresenter : NSObject <PTTranslateModuleInput, PTTranslateViewOutput, PTTranslateInteractorOutput>

@property (nonatomic, weak) id<PTTranslateViewInput> view;
@property (nonatomic, strong) id<PTTranslateInteractorInput> interactor;
@property (nonatomic, strong) id<PTTranslateRouterInput> router;

@property (nonatomic, strong) PTArticleTextBuilder *textBuilder;

@end
