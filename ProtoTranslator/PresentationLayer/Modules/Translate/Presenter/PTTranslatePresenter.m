//
//  PTTranslatePresenter.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslatePresenter.h"
#import "PTTranslateViewInput.h"
#import "PTTranslateInteractorInput.h"
#import "PTTranslateRouterInput.h"
#import "PTHistoryListModuleOutput.h"
#import "PTArticleTextBuilder.h"

@interface PTTranslatePresenter () <PTHistoryListModuleOutput>

@end

@implementation PTTranslatePresenter

#pragma mark - Методы PTTranslateModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы PTTranslateViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)didTriggerEndEditingText:(NSString *)text {
    if (text.length == 0) {
        return;
    }
    text = [text lowercaseString];
    [self.interactor translateText:text completionBlock:^(PTArticleModel *article, NSError *error) {
        if (error) {
            [self.view configureViewWithResultText:nil forWord:text];
            [self.router showErrorAlert];
        } else {
            NSAttributedString *attrString = [self.textBuilder attributedTextWithModel:article];
            [self.view configureViewWithResultText:attrString forWord:text];
        }
    }];
}

- (void)didTriggerTapHistoryButton {
    [self.router openHistoryListModuleWithOutput:self];
}

#pragma mark - Методы PTTranslateInteractorOutput

#pragma mark - PTHistoryListModuleOutput

- (void)historyListDidSelectWord:(NSString *)word {
    [self.interactor obtainCachedArticleForText:word withCompletionBlock:^(PTArticleModel *article, NSError *error) {
        if (!article) {
            //TODO: Error handling?
            return;
        }
        NSAttributedString *attrString = [self.textBuilder attributedTextWithModel:article];
        [self.view configureViewWithResultText:attrString forWord:word];
    }];
}

@end
