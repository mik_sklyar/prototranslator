//
//  PTTranslateModuleInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol PTTranslateModuleInput <RamblerViperModuleInput>

/**
 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
