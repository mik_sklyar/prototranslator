//
//  PTTranslateAssembly.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol RamblerViperModuleFactoryProtocol;

@interface PTTranslateAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<RamblerViperModuleFactoryProtocol>)factoryTranslateModule;

@end
