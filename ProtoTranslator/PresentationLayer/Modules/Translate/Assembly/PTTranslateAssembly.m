//
//  PTTranslateAssembly.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "PTTranslateAssembly.h"
#import "PTTranslateViewController.h"
#import "PTTranslateInteractor.h"
#import "PTTranslatePresenter.h"
#import "PTTranslateRouter.h"

#import "PTServiceComponents.h"
#import "PTHistoryListAssembly.h"

#import "PTArticleTextBuilder.h"

@interface PTTranslateAssembly ()

@property (nonatomic, strong, readonly) TyphoonAssembly<PTServiceComponents> *serviceComponents;
@property (nonatomic, strong, readonly) PTHistoryListAssembly *historyListAssembly;
@end

@implementation PTTranslateAssembly

- (id<RamblerViperModuleFactoryProtocol>)factoryTranslateModule {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardTranslateModule]];
                                                  [initializer injectParameterWith:@"PTTranslateViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardTranslateModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Translate"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (PTTranslateViewController *)viewTranslate {
    return [TyphoonDefinition withClass:[PTTranslateViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterTranslate]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterTranslate]];
                          }];
}

- (PTTranslateInteractor *)interactorTranslate {
    return [TyphoonDefinition withClass:[PTTranslateInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterTranslate]];
                              [definition injectProperty:@selector(translateService)
                                                    with:[self.serviceComponents translateService]];
                              [definition injectProperty:@selector(cacheService)
                                                    with:[self.serviceComponents cacheService]];
                          }];
}

- (PTTranslatePresenter *)presenterTranslate{
    return [TyphoonDefinition withClass:[PTTranslatePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewTranslate]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorTranslate]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerTranslate]];
                              [definition injectProperty:@selector(textBuilder)
                                                    with:[self textBuilderTranslate]];
                          }];
}

- (PTTranslateRouter *)routerTranslate{
    return [TyphoonDefinition withClass:[PTTranslateRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewTranslate]];
                              [definition injectProperty:@selector(historyListModuleFactory)
                                                    with:[self.historyListAssembly factoryHistoryListModule]];
                          }];
}

- (PTArticleTextBuilder *)textBuilderTranslate {
    return [TyphoonDefinition withClass:[PTArticleTextBuilder class]
                           configuration:^(TyphoonDefinition *definition) {
                               //TODO: Можно сделать компоновщик текста настраиваемым и тут передавать необходимые параметры.
                           }];
}

@end
