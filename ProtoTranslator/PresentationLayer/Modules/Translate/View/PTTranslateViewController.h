//
//  PTTranslateViewController.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PTTranslateViewInput.h"

@protocol PTTranslateViewOutput;

@interface PTTranslateViewController : UIViewController <PTTranslateViewInput>

@property (nonatomic, strong) id<PTTranslateViewOutput> output;

@end
