//
//  PTTranslateViewOutput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTTranslateViewOutput <NSObject>

/**
 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

- (void)didTriggerEndEditingText:(NSString *)text;

- (void)didTriggerTapHistoryButton;

@end
