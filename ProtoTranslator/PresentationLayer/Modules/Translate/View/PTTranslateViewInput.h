//
//  PTTranslateViewInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTTranslateViewInput <NSObject>

/**
 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

/**
 Обновляет экран

 @param resultText Статья перевода в отформатированном виде
 @param word Слово, для которого сделан перевод.
 */
- (void)configureViewWithResultText:(NSAttributedString *)resultText forWord:(NSString *)word;

@end
