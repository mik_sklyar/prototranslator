//
//  PTTranslateViewController.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateViewController.h"
#import "PTTranslateViewOutput.h"

#import "PTL10nStrings.h"

@interface PTTranslateViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UITextView *resultTextView;

@end

@implementation PTTranslateViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы PTTranslateViewInput

- (void)setupInitialState {
    self.resultTextView.text = nil;
    self.textField.placeholder = lL10nTranslatePlaceholder;
    self.title = lL10nMainScreenTitle;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                                                           target:self.output
                                                                                           action:@selector(didTriggerTapHistoryButton)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[NSString string]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)configureViewWithResultText:(NSAttributedString *)resultText forWord:(NSString *)word {
    self.resultTextView.attributedText = resultText;
    if ([[self.textField.text lowercaseString] isEqualToString:word] == NO) {
        self.textField.text = word;
    }
    [self.textField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //TODO: better handling of string from clipboard
    if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    BOOL hasText = textField.text.length > 0;
    if (hasText) {
        [self.output didTriggerEndEditingText:textField.text];
        [textField resignFirstResponder];
    }
    return hasText;
}

@end
