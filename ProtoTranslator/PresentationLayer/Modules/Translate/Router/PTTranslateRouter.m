//
//  PTTranslateRouter.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "PTTranslateRouter.h"

#import "PTL10nStrings.h"

@implementation PTTranslateRouter

#pragma mark - Методы PTTranslateRouterInput

- (void)openHistoryListModuleWithOutput:(id<PTHistoryListModuleOutput>)moduleOutput {
    [[self.transitionHandler openModuleUsingFactory:self.historyListModuleFactory
                                withTransitionBlock:^(id <RamblerViperModuleTransitionHandlerProtocol> sourceModuleTransitionHandler, id <RamblerViperModuleTransitionHandlerProtocol> destinationModuleTransitionHandler) {

                                    UIViewController *sourceViewController = (id)sourceModuleTransitionHandler;
                                    UIViewController *destinationViewController = (id)destinationModuleTransitionHandler;
                                    [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];

                                }] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<RamblerViperModuleInput> moduleInput) {
                                    return moduleOutput;
                                }];
}


- (void)showErrorAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kL10nErrorTitle
                                                                message:kL10nErrorMessageCantTranslate preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:kL10nOK
                                           style:UIAlertActionStyleCancel
                                         handler:nil]];

    UIViewController *sourceViewController = (id)self.transitionHandler;
    [sourceViewController presentViewController:alertController animated:YES completion:nil];
}

@end
