//
//  PTTranslateRouterInput.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTHistoryListModuleOutput;

@protocol PTTranslateRouterInput <NSObject>

/**
 Открывает модуль списка переведённых слов
 */
- (void)openHistoryListModuleWithOutput:(id<PTHistoryListModuleOutput>)moduleOutput;

/**
 Показывает диалог ошибки перевода
 */
- (void)showErrorAlert;

@end
