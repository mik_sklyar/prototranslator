//
//  PTTranslateRouter.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "PTTranslateRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface PTTranslateRouter : NSObject <PTTranslateRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@property (nonatomic, strong) id<RamblerViperModuleFactoryProtocol> historyListModuleFactory;

@end
