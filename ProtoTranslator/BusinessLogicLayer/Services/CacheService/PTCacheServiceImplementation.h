//
//  PTCacheServiceImplementation.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 24/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTCacheService.h"

@class YapDatabase;

@interface PTCacheServiceImplementation : NSObject <PTCacheService>

@property (nonatomic, strong) YapDatabase *database;

@end
