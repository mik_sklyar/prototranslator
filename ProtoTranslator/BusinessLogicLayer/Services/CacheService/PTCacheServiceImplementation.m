//
//  PTCacheServiceImplementation.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 24/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTCacheServiceImplementation.h"
#import <YapDatabase/YapDatabase.h>
#import "PTArticleModel.h"

static NSString *kArticlesCollectionName = @"Articles";

@interface PTCacheServiceImplementation ()

@property (nonatomic, strong) YapDatabaseConnection *readConnection;
@property (nonatomic, strong) YapDatabaseConnection *writeConnection;

@end

@implementation PTCacheServiceImplementation

- (void)setDatabase:(YapDatabase *)database {
    _database = database;

    self.readConnection = [database newConnection];
    self.writeConnection = [database newConnection];
}

- (void)saveArticle:(PTArticleModel *)articleModel {
    if (articleModel.title == nil) {
        return;
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            [self.writeConnection readWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
                [transaction setObject:articleModel
                                forKey:articleModel.title
                          inCollection:kArticlesCollectionName];
            }];
        }
    });
}

- (void)removeArticle:(PTArticleModel *)articleModel {
    if (articleModel.title == nil) {
        return;
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            [self.writeConnection readWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
                [transaction removeObjectForKey:articleModel.title inCollection:kArticlesCollectionName];
            }];
        }
    });
}

- (void)loadArticleByWord:(NSString *)word completionBlock:(void (^)(PTArticleModel *articleModel, NSError *error))completion {
    [self.readConnection readWithBlock:^(YapDatabaseReadTransaction * _Nonnull transaction) {
        @autoreleasepool {
            NSError *error = nil; //TODO: generate error if needed

            PTArticleModel *articleModel = [transaction objectForKey:word inCollection:kArticlesCollectionName];

            if (completion) {
                completion(articleModel, error);
            }
        }
    }];
}

- (void)loadAllArticlesKeysWithCompletionBlock:(void (^)(NSArray<NSString *> *articlesKeys, NSError *error))completion {
    [self.readConnection readWithBlock:^(YapDatabaseReadTransaction * _Nonnull transaction) {
        @autoreleasepool {
            NSError *error = nil; //TODO: generate error if needed

            NSArray<NSString *> *articlesKeys = [transaction allKeysInCollection:kArticlesCollectionName];

            if (completion) {
                completion(articlesKeys, error);
            }
        }
    }];
}

@end
