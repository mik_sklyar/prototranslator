//
//  PTCacheService.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 24/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PTArticleModel;

/**
 Простой сервис для сохранения истории переводов.
 */
@protocol PTCacheService <NSObject>

- (void)saveArticle:(PTArticleModel *)articleModel;

- (void)removeArticle:(PTArticleModel *)articleModel;

- (void)loadArticleByWord:(NSString *)word completionBlock:(void (^)(PTArticleModel *articleModel, NSError *error))completion;

- (void)loadAllArticlesKeysWithCompletionBlock:(void (^)(NSArray<NSString *> *articlesKeys, NSError *error))completion;

@end
