//
//  PTAuthService.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTAuthService <NSObject>

- (NSString *)bearerToken;

- (void)authorizeWithCompletionBlock:(void (^)(NSError *error))completion;

@end
