//
//  PTAuthServiceImplementation.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTAuthServiceImplementation.h"
#import "PTAPIConstants.h"

#import <AFNetworking/AFHTTPSessionManager.h>

static NSString *const kAuthURL = @"api/v1.1/authenticate";
static NSString *const kBearerTokenKey = @"PTBearerTokenKey";

@interface PTAuthServiceImplementation ()

@property (nonatomic, strong) NSString *bearerToken;

@end

@implementation PTAuthServiceImplementation

- (void)setBearerToken:(NSString *)bearerToken {
    [[NSUserDefaults standardUserDefaults] setObject:bearerToken forKey:kBearerTokenKey];
}

- (NSString *)bearerToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kBearerTokenKey];
}

- (void)authorizeWithCompletionBlock:(void (^)(NSError *error))completion {
    [self.requestManager.requestSerializer setValue:[NSString stringWithFormat:kAuthBasicFormat, self.authToken]
                                 forHTTPHeaderField:kAuthHeaderKey];
    self.requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];

    NSString *urlString = [[NSURL URLWithString:kAuthURL relativeToURL:self.requestManager.baseURL] absoluteString];
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.requestManager.requestSerializer requestWithMethod:@"POST"
                                                                                  URLString:urlString
                                                                                 parameters:nil                                                                      error:&serializationError];
    if (serializationError) {
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(serializationError);
            });
        }
        return;
    }
    [[self.requestManager dataTaskWithRequest:request
                               uploadProgress:nil
                             downloadProgress:nil
                            completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                if ([(NSHTTPURLResponse *)response statusCode] == 200) {
                                    self.bearerToken = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                }
                                if (completion) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        completion(error);
                                    });
                                }
                            }] resume];
}

@end
