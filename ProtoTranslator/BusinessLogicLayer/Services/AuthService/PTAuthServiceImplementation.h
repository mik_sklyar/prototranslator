//
//  PTAuthServiceImplementation.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTAuthService.h"

@class AFHTTPSessionManager;

@interface PTAuthServiceImplementation : NSObject <PTAuthService>

@property (nonatomic, copy) NSString *authToken;

@property (nonatomic, strong) AFHTTPSessionManager *requestManager;

@end
