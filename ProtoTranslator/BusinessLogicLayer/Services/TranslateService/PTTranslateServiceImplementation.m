//
//  PTTranslateService.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateServiceImplementation.h"
#import "PTAuthService.h"

#import <AFNetworking/AFHTTPSessionManager.h>
#import "PTAPIConstants.h"

#import <EasyMapping/EasyMapping.h>
#import "PTArticleModel.h"

static NSString *const kTranslateURL = @"/api/v1/Article";
static NSString *const kTranslateParamText = @"heading";
static NSString *const kTranslateParanDict = @"dict";
static NSString *const kTranslateParamFromLang = @"srcLang";
static NSString *const kTranslateParamToLang = @"dstLang";

@implementation PTTranslateServiceImplementation

- (void)translateText:(NSString *)text
           dictionary:(NSString *)dictionaryName
       fromLanguageID:(NSNumber *)fromLang
         toLanguageID:(NSNumber *)toLang
      completionBlock:(void (^)(PTArticleModel *article, NSError *error))completion {

    void (^authCompletionBlock)(NSError *) = ^(NSError *error) {
        if (error) {
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
            }
        } else {
            [self translateText:text
                     dictionary:dictionaryName
                 fromLanguageID:fromLang
                   toLanguageID:toLang
                completionBlock:completion];
        }
    };

    NSString *bearerToken = self.authService.bearerToken;
    if (bearerToken.length == 0) {
        [self.authService authorizeWithCompletionBlock:authCompletionBlock];
        return;
    }

    [self.requestManager.requestSerializer setValue:[NSString stringWithFormat:kAuthBarierFormat, bearerToken]
                                 forHTTPHeaderField:kAuthHeaderKey];
    self.requestManager.responseSerializer = [AFJSONResponseSerializer serializer];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kTranslateParamText] = text;
    params[kTranslateParanDict] = dictionaryName;
    params[kTranslateParamFromLang] = fromLang;
    params[kTranslateParamToLang] = toLang;

    NSError *serializationError = nil;
    NSString *urlString = [[NSURL URLWithString:kTranslateURL relativeToURL:self.requestManager.baseURL] absoluteString];
    NSMutableURLRequest *request = [self.requestManager.requestSerializer requestWithMethod:@"GET"
                                                                                  URLString:urlString
                                                                                 parameters:params                                                                      error:&serializationError];
    if (serializationError) {
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, serializationError);
            });
        }
        return;
    }
    [[self.requestManager dataTaskWithRequest:request
                               uploadProgress:nil
                             downloadProgress:nil
                            completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                if ([(NSHTTPURLResponse *)response statusCode] == 401) {
                                    [self.authService authorizeWithCompletionBlock:authCompletionBlock];
                                    return;
                                }

                                if (completion) {
                                    if (error != nil) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            completion(nil, error);
                                        });
                                    } else {
                                        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                                        PTArticleModel *article = [EKMapper objectFromExternalRepresentation:responseDictionary
                                                                                                 withMapping:[PTArticleModel objectMapping]];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            completion(article, nil);
                                        });
                                    }
                                }
                            }] resume];

}

@end
