//
//  PTTranslateServiceImplementation.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTTranslateService.h"

@protocol PTAuthService;
@class AFHTTPSessionManager;

@interface PTTranslateServiceImplementation : NSObject <PTTranslateService>

@property (nonatomic, strong) id<PTAuthService> authService;
@property (nonatomic, strong) AFHTTPSessionManager *requestManager;

@end
