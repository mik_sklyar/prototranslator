//
//  PTTranslateService.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PTArticleModel;

@protocol PTTranslateService <NSObject>

- (void)translateText:(NSString *)text
           dictionary:(NSString *)dictionaryName
       fromLanguageID:(NSNumber *)fromLang
         toLanguageID:(NSNumber *)toLang
      completionBlock:(void (^)(PTArticleModel *article, NSError *error))completion;

@end
