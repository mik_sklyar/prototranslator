//
//  PTServiceComponentsAssembly.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

#import "PTServiceComponents.h"

@interface PTServiceComponentsAssembly : TyphoonAssembly <RamblerInitialAssembly, PTServiceComponents>

@end
