//
//  PTApplicationAssembly.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "PTApplicationAssembly.h"
#import "PTTranslateAssembly.h"

#import "AppDelegate.h"

@interface PTApplicationAssembly ()

@property (nonatomic, strong, readonly) PTTranslateAssembly *translateAssembly;

@end

@implementation PTApplicationAssembly

#pragma mark - Bootstrapping

- (AppDelegate *)appDelegate {
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(window) with:[self mainWindow]];
    }];
}

#pragma mark - Private

- (UIWindow *)mainWindow {
    return [TyphoonDefinition withClass:[UIWindow class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithFrame:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]]];
        }];
        [definition injectProperty:@selector(rootViewController) with:[self rootViewController]];
    }];
}

- (UIViewController *)rootViewController {
    return [TyphoonDefinition withClass:[UINavigationController class] configuration:^(TyphoonDefinition *definition) {
                [definition useInitializer:@selector(initWithRootViewController:) parameters:^(TyphoonMethod *initializer) {
                    [initializer injectParameterWith:[self mainViewController]];
                 }];
            }];
}

- (UIViewController *)mainViewController {
        return [TyphoonDefinition withFactory:[self.translateAssembly factoryTranslateModule] selector:@selector(instantiateModuleTransitionHandler)];
}

@end
