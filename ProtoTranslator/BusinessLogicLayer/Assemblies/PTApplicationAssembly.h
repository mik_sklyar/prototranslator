//
//  PTApplicationAssembly.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 12/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@class AppDelegate;

@interface PTApplicationAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (AppDelegate *)appDelegate;

@end
