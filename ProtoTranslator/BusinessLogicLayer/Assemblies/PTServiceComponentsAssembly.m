//
//  PTServiceComponentsAssembly.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTServiceComponentsAssembly.h"

#import "PTAuthServiceImplementation.h"
#import "PTTranslateServiceImplementation.h"

#import <AFNetworking/AFNetworking.h>
#import "PTAPIConstants.h"

#import <YapDatabase/YapDatabase.h>
#import "PTCacheServiceImplementation.h"

@implementation PTServiceComponentsAssembly

- (id<PTAuthService>)authService {
    return [TyphoonDefinition withClass:[PTAuthServiceImplementation class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(authToken) with:kAuthToken];
                              [definition injectProperty:@selector(requestManager) with:[self httpManager]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (id<PTTranslateService>)translateService {
    return [TyphoonDefinition withClass:[PTTranslateServiceImplementation class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(authService) with:[self authService]];
                              [definition injectProperty:@selector(requestManager) with:[self httpManager]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (AFHTTPSessionManager *)httpManager {
    return [TyphoonDefinition withClass:[AFHTTPSessionManager class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithSessionConfiguration:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSURLSessionConfiguration defaultSessionConfiguration]];
        }];
        [definition injectProperty:@selector(baseURL) with:[NSURL URLWithString:kBaseURL]];
        [definition injectProperty:@selector(requestSerializer) with:[AFJSONRequestSerializer serializer]];
    }];
}

- (id<PTCacheService>)cacheService {
    return [TyphoonDefinition withClass:[PTCacheServiceImplementation class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(database) with:[self cacheDatabase]];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (YapDatabase *)cacheDatabase {
    YapDatabasePolicy objectPolicy = YapDatabasePolicyCopy;
    NSValue *objectPolicyValue = [NSValue value:&objectPolicy withObjCType:@encode(typeof(objectPolicy))];
    return [TyphoonDefinition withClass:[YapDatabase class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithPath:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self databasePath]];
                                              }];
                              [definition injectProperty:@selector(defaultObjectPolicy) with:objectPolicyValue];
                              [definition injectProperty:@selector(defaultMetadataPolicy) with:objectPolicyValue];
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (NSString *)databasePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *baseDir = ([paths count] > 0) ? paths[0] : NSTemporaryDirectory();

    NSString *dbFileName = @"ptcache.sqlite";
    NSString *dbPath = [baseDir stringByAppendingPathComponent:dbFileName];

    return dbPath;
}

@end
