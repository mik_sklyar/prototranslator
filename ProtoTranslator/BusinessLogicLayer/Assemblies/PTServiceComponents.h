//
//  PTServiceComponents.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 16/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PTAuthService;
@protocol PTTranslateService;
@protocol PTCacheService;

@protocol PTServiceComponents <NSObject>

- (id<PTAuthService>)authService;

- (id<PTTranslateService>)translateService;

- (id<PTCacheService>)cacheService;

@end
