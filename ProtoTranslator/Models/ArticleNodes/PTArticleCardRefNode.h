//
//  PTArticleCardRefNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 24/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleCardRefNode : PTArticleNode

/**
 Словарь ссылки на карточку
 */
@property (nonatomic, copy) NSString *dictionaryName;

/**
 Идентификатор статьи.
 */
@property (nonatomic, copy) NSString *articleId;

@end
