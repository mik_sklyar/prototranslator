//
//  PTArticleNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@implementation PTArticleNode

- (NSString *)description {
    return self.text;
}

- (NSString *)debugDescription {
    return [NSString stringWithFormat:@"%@: type=%lu, text=\"%@\"",
            NSStringFromClass([self class]),
            (unsigned long)self.nodeType,
            self.text];
}

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[self class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Node" toProperty:@"nodeType" withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
            return [self nodeTypeWithString:value];
        }];
        [mapping mapKeyPath:@"Text" toProperty:@"text"];
        [mapping mapKeyPath:@"IsOptional" toProperty:@"isOptional"];
    }];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        self.nodeType = [[decoder decodeObjectForKey:@"nodeType"] integerValue];
        self.text = [decoder decodeObjectForKey:@"text"];
        self.isOptional = [[decoder decodeObjectForKey:@"isOptional"] boolValue];;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:@(self.nodeType) forKey:@"nodeType"];
    [encoder encodeObject:self.text forKey:@"text"];
    [encoder encodeObject:@(self.isOptional) forKey:@"isOptional"];
}

#pragma mark - Private

+ (NSNumber *)nodeTypeWithString:(NSString *)nodeTypeString {
    static dispatch_once_t onceToken;
    static NSDictionary *nodeTypes = nil;
    dispatch_once(&onceToken, ^{
        nodeTypes = @{
                      @"Comment": @(PTArticleNodeTypeComment),
                      @"Paragraph": @(PTArticleNodeTypeParagraph),
                      @"Text": @(PTArticleNodeTypeText),
                      @"List": @(PTArticleNodeTypeList),
                      @"ListItem": @(PTArticleNodeTypeListItem),
                      @"Examples": @(PTArticleNodeTypeExamples),
                      @"ExampleItem": @(PTArticleNodeTypeExampleItem),
                      @"Example": @(PTArticleNodeTypeExample),
                      @"CardRefs": @(PTArticleNodeTypeCardRefs),
                      @"CardRefItem": @(PTArticleNodeTypeCardRefItem),
                      @"CardRef": @(PTArticleNodeTypeCardRef),
                      @"Transcription": @(PTArticleNodeTypeTranscription),
                      @"Abbrev": @(PTArticleNodeTypeAbbrev),
                      @"Caption": @(PTArticleNodeTypeCaption),
                      @"Sound": @(PTArticleNodeTypeSound),
                      @"Ref": @(PTArticleNodeTypeRef),
                      };
    });
    return nodeTypes[nodeTypeString] ?: @(PTArticleNodeTypeUnsupported);
}

@end
