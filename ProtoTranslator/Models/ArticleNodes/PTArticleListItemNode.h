//
//  PTArticleListItemNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleListItemNode : PTArticleNode

/**
 Разметка пункта в списке.
 */
@property (nonatomic, strong) NSArray<PTArticleNode *> *markup;

@end
