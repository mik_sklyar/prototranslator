//
//  PTArticleAbbrevNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleAbbrevNode.h"

@implementation PTArticleAbbrevNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"FullText" toProperty:@"fullText"];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.fullText = [decoder decodeObjectForKey:@"fullText"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.fullText forKey:@"fullText"];
}

@end
