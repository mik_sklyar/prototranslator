//
//  PTArticleNode+EasyMapping.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

typedef id _Nullable(^PTValueMappingBlock)(NSString * _Nonnull key, id  _Nullable value);

@interface PTArticleNode (EasyMapping)

+ (PTValueMappingBlock)fabricMappingBlock;

@end
