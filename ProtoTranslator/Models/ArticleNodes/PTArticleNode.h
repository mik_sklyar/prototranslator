//
//  PTArticleNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

typedef NS_ENUM(NSUInteger, PTArticleNodeType) {
    PTArticleNodeTypeComment = 0,   //Комментарий
    PTArticleNodeTypeParagraph,     //Абзац
    PTArticleNodeTypeText,          //Простой текст
    PTArticleNodeTypeList,          //Список
    PTArticleNodeTypeListItem,      //Элемент списка
    PTArticleNodeTypeExamples,      //Примеры
    PTArticleNodeTypeExampleItem,   //Элемент списка примеров
    PTArticleNodeTypeExample,       //Пример
    PTArticleNodeTypeCardRefs,      //Ссылки на карточки
    PTArticleNodeTypeCardRefItem,   //Элемент списка ссылок на карточки
    PTArticleNodeTypeCardRef,       //Ссылка на карточку
    PTArticleNodeTypeTranscription, //Транскрипция
    PTArticleNodeTypeAbbrev,        //Аббревиатура
    PTArticleNodeTypeCaption,       //Заголовок
    PTArticleNodeTypeSound,         //Ссылка на звуковой файл
    PTArticleNodeTypeRef,           //Ссылка
    PTArticleNodeTypeUnsupported,   //Неподдерживаемый тип элемента
};

@interface PTArticleNode : NSObject <EKMappingProtocol, NSCoding>

/**
 Тип узла в словарной статье.
 */
@property (nonatomic, assign) PTArticleNodeType nodeType;

/**
 Текст.
 */
@property (nonatomic, copy) NSString *text;

/**
 Является ли опциональным.
 */
@property (nonatomic, assign) BOOL isOptional;

@end
