//
//  PTArticleListNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleListNode.h"
#import "PTArticleNode+EasyMapping.h"

@implementation PTArticleListNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"Type" toProperty:@"listType"];
    [mapping mapKeyPath:@"Items" toProperty:@"listItems" withValueBlock:[PTArticleNode fabricMappingBlock]];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.listType = [[decoder decodeObjectForKey:@"listType"] integerValue];
        self.listItems = [decoder decodeObjectForKey:@"listItems"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:@(self.listType) forKey:@"listType"];
    [encoder encodeObject:self.listItems forKey:@"listItems"];
}

@end
