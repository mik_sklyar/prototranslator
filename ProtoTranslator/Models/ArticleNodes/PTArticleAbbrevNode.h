//
//  PTArticleAbbrevNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleAbbrevNode : PTArticleNode

/**
 Полный текст сокращения.
 */
@property (nonatomic, copy) NSString *fullText;

@end
