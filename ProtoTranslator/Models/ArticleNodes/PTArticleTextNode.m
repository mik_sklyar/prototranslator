//
//  PTArticleTextNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleTextNode.h"

@implementation PTArticleTextNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"IsItalics" toProperty:@"isItalic"];
    [mapping mapKeyPath:@"IsAccent" toProperty:@"isAccent"];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.isItalic = [[decoder decodeObjectForKey:@"isItalic"] boolValue];
        self.isAccent = [[decoder decodeObjectForKey:@"isAccent"] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:@(self.isItalic) forKey:@"isItalic"];
    [encoder encodeObject:@(self.isAccent) forKey:@"isAccent"];
}

@end
