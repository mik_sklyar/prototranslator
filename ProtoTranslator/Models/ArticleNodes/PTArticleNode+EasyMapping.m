//
//  PTArticleNode+EasyMapping.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode+EasyMapping.h"

#import "PTArticleTextNode.h"
#import "PTArticleMarkupNode.h"
#import "PTArticleAbbrevNode.h"
#import "PTArticleSoundNode.h"

#import "PTArticleListNode.h"
#import "PTArticleListItemNode.h"

#import "PTArticleCardRefNode.h"

@implementation PTArticleNode (EasyMapping)

+ (PTValueMappingBlock)fabricMappingBlock {
    PTValueMappingBlock fabricBlock = ^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
        if ([value isKindOfClass:[NSDictionary class]]) {
            return [self articleNodeWithProperties:value];
        }
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *array = value;
            NSMutableArray *resultArray = [NSMutableArray array];
            for (NSDictionary *dictionary in array) {
                PTArticleNode *node = [self articleNodeWithProperties:dictionary];
                if (node) {
                    [resultArray addObject:node];
                }
            }
            if (resultArray.count > 0) {
                return [resultArray copy];
            }
        }
        return nil;
    };
    return fabricBlock;
}

+ (PTArticleNode *)articleNodeWithProperties:(NSDictionary *)properties {
    if (![properties isKindOfClass:[NSDictionary class]]) {
        return nil;
    }

    Class nodeClass = [self classForNodeTypeString:properties[@"Node"]];
    if (nodeClass == nil) {
        return nil;
    }

    PTArticleNode *node = [EKMapper objectFromExternalRepresentation:properties
                                                         withMapping:[nodeClass objectMapping]];
    return node;
}

+ (Class)classForNodeTypeString:(NSString *)nodeTypeString {
    static dispatch_once_t onceToken;
    static NSDictionary *nodeClasses = nil;
    dispatch_once(&onceToken, ^{
        nodeClasses = @{
                      @"Comment": [PTArticleMarkupNode class],
                      @"Paragraph": [PTArticleMarkupNode class],
                      @"Text": [PTArticleTextNode class],
                      @"List": [PTArticleListNode class],
                      @"ListItem": [PTArticleListItemNode class],
                      @"Examples": [PTArticleListNode class],
                      @"ExampleItem": [PTArticleListItemNode class],
                      @"Example": [PTArticleMarkupNode class],
                      @"CardRefs": [PTArticleListNode class],
                      @"CardRefItem": [PTArticleListItemNode class],
                      @"CardRef": [PTArticleCardRefNode class],
                      @"Transcription": [PTArticleNode class],
                      @"Abbrev": [PTArticleAbbrevNode class],
                      @"Caption": [PTArticleNode class],
                      @"Sound": [PTArticleSoundNode class],
//                      @"Ref": [PTArticleNode class],
                      };
    });
    return nodeClasses[nodeTypeString];
}
@end
