//
//  PTArticleSoundNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleSoundNode.h"

@implementation PTArticleSoundNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"FileName" toProperty:@"soundFileName"];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.soundFileName = [decoder decodeObjectForKey:@"soundFileName"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.soundFileName forKey:@"soundFileName"];
}

@end
