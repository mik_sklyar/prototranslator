//
//  PTArticleMarkupNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleMarkupNode.h"
#import "PTArticleNode+EasyMapping.h"

@implementation PTArticleMarkupNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"Markup" toProperty:@"markup" withValueBlock:[PTArticleNode fabricMappingBlock]];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.markup = [decoder decodeObjectForKey:@"markup"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.markup forKey:@"markup"];
}

@end
