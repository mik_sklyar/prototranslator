//
//  PTArticleCardRefNode.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 24/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleCardRefNode.h"

@implementation PTArticleCardRefNode

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping *mapping = [super objectMapping];

    [mapping mapKeyPath:@"Dictionary" toProperty:@"dictionaryName"];
    [mapping mapKeyPath:@"ArticleId" toProperty:@"articleId"];

    return mapping;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        self.dictionaryName = [decoder decodeObjectForKey:@"dictionaryName"];
        self.articleId = [decoder decodeObjectForKey:@"articleId"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.dictionaryName forKey:@"dictionaryName"];
    [encoder encodeObject:self.articleId forKey:@"articleId"];
}
@end
