//
//  PTArticleMarkupNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleMarkupNode : PTArticleNode

/**
 Разметка узла.
 */
@property (nonatomic, strong) NSArray<PTArticleNode *> *markup;

@end
