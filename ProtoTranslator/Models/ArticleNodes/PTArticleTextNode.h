//
//  PTArticleTextNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleTextNode : PTArticleNode

/**
 Выделять курсивом.
 */
@property (nonatomic, assign) BOOL isItalic;

/**
 Устаревшее? Акцент? :D
 */
@property (nonatomic, assign) BOOL isAccent;

@end
