//
//  PTArticleSoundNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleSoundNode : PTArticleNode

/**
 Имя файла со звуком
 */
@property (nonatomic, copy) NSString *soundFileName;

@end
