//
//  PTArticleListNode.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 18/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleNode.h"

@interface PTArticleListNode : PTArticleNode

/**
 Тип списка
 */
@property (nonatomic, assign) NSInteger listType;

/**
 Пункты списка.
 */
@property (nonatomic, strong) NSArray<PTArticleNode *> *listItems;

@end
