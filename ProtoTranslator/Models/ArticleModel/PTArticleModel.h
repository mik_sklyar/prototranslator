//
//  PTArticleModel.h
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EasyMapping/EasyMapping.h>

@class PTArticleNode;

@interface PTArticleModel : NSObject <EKMappingProtocol, NSCoding>

/**
 Заголовок словарной статьи.
 */
@property (nonatomic, copy) NSString *title;

/**
 Разметка заголовка. Может использоваться, например, для расстановки ударений.
 */
@property (nonatomic, strong) NSArray<PTArticleNode *> *titleMarkup;

/**
 Словарь, к которому относится данная словарная статья.
 */
@property (nonatomic, copy) NSString *dictionaryName;

/**
 Идентификатор статьи.
 */
@property (nonatomic, copy) NSString *articleId;

/**
 Тело статьи.
 */
@property (nonatomic, strong) NSArray<PTArticleNode *> *bodyNodes;

@end
