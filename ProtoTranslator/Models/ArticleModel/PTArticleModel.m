//
//  PTArticleModel.m
//  ProtoTranslator
//
//  Created by Mikhail Sklyar on 17/09/2018.
//  Copyright © 2018 mik_sklyar. All rights reserved.
//

#import "PTArticleModel.h"
#import "PTArticleNode.h"
#import "PTArticleNode+EasyMapping.h"

@implementation PTArticleModel

#pragma mark - EKMappingProtocol

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[self class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"Title" toProperty:@"title"];
        [mapping mapKeyPath:@"Dictionary" toProperty:@"dictionaryName"];
        [mapping mapKeyPath:@"ArticleId" toProperty:@"articleId"];

        [mapping mapKeyPath:@"TitleMarkup" toProperty:@"titleMarkup" withValueBlock:[PTArticleNode fabricMappingBlock]];
        [mapping mapKeyPath:@"Body" toProperty:@"bodyNodes" withValueBlock:[PTArticleNode fabricMappingBlock]];
    }];
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        self.title = [decoder decodeObjectForKey:@"title"];
        self.dictionaryName = [decoder decodeObjectForKey:@"dictionaryName"];
        self.articleId = [decoder decodeObjectForKey:@"articleId"];
        self.titleMarkup = [decoder decodeObjectForKey:@"titleMarkup"];
        self.bodyNodes = [decoder decodeObjectForKey:@"bodyNodes"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.dictionaryName forKey:@"dictionaryName"];
    [encoder encodeObject:self.articleId forKey:@"articleId"];
    [encoder encodeObject:self.titleMarkup forKey:@"titleMarkup"];
    [encoder encodeObject:self.bodyNodes forKey:@"bodyNodes"];
}

@end
